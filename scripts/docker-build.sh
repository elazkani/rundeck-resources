#!/usr/bin/bash

VERSION=$(cat rundeck_resources/__init__.py | cut -d ' ' -f 3 | cut -d "'" -f 2)

docker build -t elazkani/rundeck-resources .
docker tag elazkani/rundeck-resources:latest elazkani/rundeck-resources:$VERSION
