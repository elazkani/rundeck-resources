#!/bin/bash

coverage run --source=rundeck_resources -m unittest discover -b tests/
coverage report -m
mypy rundeck_resources/ --ignore-missing-imports
