#!/usr/bin/bash

VERSION=$(cat rundeck_resources/__init__.py | cut -d ' ' -f 3 | cut -d "'" -f 2)

docker push elazkani/rundeck-resources:latest
docker push elazkani/rundeck-resources:$VERSION
