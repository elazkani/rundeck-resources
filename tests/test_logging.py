import unittest
import logging
from unittest import mock
from rundeck_resources.common import check_file
from rundeck_resources.logger import setup_logging


class TestLogger(unittest.TestCase):
    @mock.patch('rundeck_resources.logger.logging.basicConfig')
    @mock.patch('rundeck_resources.logger.logging.config.dictConfig')
    @mock.patch('rundeck_resources.logger.yaml.safe_load')
    @mock.patch('rundeck_resources.logger.open',
                new_callable=mock.mock_open(read_data="{}"), create=True)
    @mock.patch('rundeck_resources.logger.check_file')
    @mock.patch('rundeck_resources.logger.os.getenv')
    def test_setup_logging(self, mock_ge, mock_cf, mock_o,
                           mock_ysl, mock_dc, mock_bc):
        # Test no configuration file
        mock_ge.return_value = None
        setup_logging(None, logging.ERROR)
        _format = '%(asctime)s - %(levelname)s - %(filename)s:' \
            '%(name)s.%(funcName)s:%(lineno)d - %(message)s'
        mock_bc.assert_called_with(level=logging.ERROR, format=_format)

        # Default values
        default_path = "/some/path"
        mock_ysl_return_value = "--- {}"

        # Test environment variable config path
        mock_ge.return_value = default_path
        mock_cf.return_value = default_path
        mock_ysl.return_value = mock_ysl_return_value
        mocked_file = mock_o.return_value.__enter__.return_value.read()
        setup_logging(None, logging.ERROR)
        mock_ysl.assert_called_with(mocked_file)
        mock_dc.assert_called_with(mock_ysl_return_value)

        # Test providing a config file path
        mock_cf.return_value = default_path
        mock_ysl.return_value = mock_ysl_return_value
        mocked_file = mock_o.return_value.__enter__.return_value.read()
        setup_logging(default_path, logging.ERROR)
        mock_ysl.assert_called_with(mocked_file)
        mock_dc.assert_called_with(mock_ysl_return_value)

        # Test with wrong config path
        mock_cf.side_effect = check_file
        setup_logging(default_path, logging.ERROR)
        mock_bc.assert_called_with(level=logging.ERROR, format=_format)

        # Test with wrong env_var config path
        mock_ge.return_value = default_path
        setup_logging(None, logging.ERROR)
        mock_bc.assert_called_with(level=logging.ERROR, format=_format)
