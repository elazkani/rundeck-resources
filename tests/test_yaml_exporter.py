import unittest
from unittest import mock
from rundeck_resources.yaml_exporter import YAMLExporter


class TestYAMLExporter(unittest.TestCase):
    def setUp(self):
        self.title = 'YAML'
        self.config = {
            'YAMLExporter:YAML': {
                'export_path': '/some/path'
            }
        }
        self.dictionary = {
            'node': {
                'fdqn': 'node.example.com'
            }
        }

        self.yaml_class = YAMLExporter(self.title, self.config)

    @mock.patch('rundeck_resources.yaml_exporter.open',
                new_callable=mock.mock_open(), create=True)
    @mock.patch('yaml.dump')
    def test_export_resources_file(self, mock_yd, mock_o):
        self.yaml_class.export_resources(self.dictionary)
        mock_o.assert_called_with('/some/path', 'w+')
        stream = mock_o.return_value.__enter__.return_value
        mock_yd.assert_called_with(self.dictionary,
                                   stream=stream,
                                   explicit_start=True,
                                   default_flow_style=False)

    @mock.patch('yaml.dump')
    def test_export_resources_no_file(self, mock_yd):
        self.yaml_class.config = None
        self.yaml_class.export_resources(self.dictionary)
        mock_yd.assert_called_with(self.dictionary,
                                   explicit_start=True,
                                   default_flow_style=False)

    def test_export_path(self):
        result = self.yaml_class.export_path(self.yaml_class.config)
        self.assertEqual(result, '/some/path')

        result = self.yaml_class.export_path('')
        self.assertEqual(result, '')
