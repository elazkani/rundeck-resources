import unittest
from unittest import mock
from rundeck_resources.plugins import load_plugins
from rundeck_resources.plugins import get_plugins
from rundeck_resources.errors import ConfigError

config = {'YAMLExporter:YAML': {},
          'ChefImporter:ChefServer': {}}

entry_point = {
    'Importers': [
        'ChefImporter = rundeck_resources.chef_importer:ChefImporter'
    ],
    'Exporters': [
        'YAMLExporter = rundeck_resources.yaml_exporter:YAMLExporter'
    ]}


class PKGResource:
    def __init__(self, resource):
        self.resource = resource
        self.name = resource.split('=')[0].rstrip()

    def load(self):
        return True

    def __repr__(self):
        return repr(self.resource)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


def iter_entry_points(resource_types):
    for resource_type in resource_types:
        yield PKGResource(resource_type)


class TestPlugins(unittest.TestCase):
    def setUp(self):
        self.res_types = 'Type = type'
        self.config = {
            'Type:type': self.res_types
        }

    @mock.patch('rundeck_resources.plugins.get_plugins')
    def test_load_plugins(self, mock_gp):
        resource = PKGResource(self.res_types)
        mock_gp_result = {resource.name: resource}
        mock_gp.return_value = mock_gp_result
        result = load_plugins(self.config, self.res_types)
        expected_result = {'Type': {
            'plugin': True,
            'title': 'type'}}
        self.assertEqual(result, expected_result)
        config = {'Type': self.res_types}
        self.assertRaises(ConfigError,
                          load_plugins, config, self.res_types)

    @mock.patch('rundeck_resources.plugins.pkg_resources.iter_entry_points',
                side_effect=iter_entry_points)
    def test_get_plugins(self, mock_pr):
        result = get_plugins([self.res_types])
        resource = PKGResource(self.res_types)
        expected_result = {resource.name: resource}
        self.assertEqual(result, expected_result)
