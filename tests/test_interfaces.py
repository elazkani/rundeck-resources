import unittest
from rundeck_resources.interfaces import ResourcesImporter
from rundeck_resources.interfaces import ResourcesExporter


class TestResourcesImporter(ResourcesImporter, unittest.TestCase):
    def test_resources_importer(self):
        self.assertIsInstance(self, ResourcesImporter)
        self.assertEqual(ResourcesImporter.import_resources(self), None)

    def import_resources(self):
        pass


class TestResourcesExporter(ResourcesExporter, unittest.TestCase):
    def test_resources_exporter(self):
        self.assertIsInstance(self, ResourcesExporter)
        self.assertEqual(ResourcesExporter.export_resources(self, {}), None)

    def export_resources(self, resources):
        pass
