import unittest
from unittest import mock
from rundeck_resources.chef_importer import ChefImporter
from rundeck_resources.common import check_file
from rundeck_resources.errors import CacheNotFound


config = {
    'ChefImporter:ChefServer': {
        'user_cert_path': '/',
        'ssl_cert_path': '/',
        'url': 'https://api.chef.io',
        'user': 'unittest',
        'version': '12.10.2'
    }
}

config_no_ssl = {
    'user_cert_path': '/',
    'url': 'https://api.chef.io',
    'user': 'unittest',
    'version': '12.10.2'
}

config_no_version = {
    'user_cert_path': '/',
    'ssl_cert_path': '/',
    'url': 'https://api.chef.io',
    'user': 'unittest'
}

config_no_ssl_no_version = {
    'user_cert_path': '/',
    'url': 'https://api.chef.io',
    'user': 'unittest'
}

config_rundeck_user_login = {
    'rundeck_user_login': 'rundeck'
}

chef_value = [{
    'name': 'name',
    'chef_environment': 'unittest',
    'normal': {
        'tags': ['tag1', 'tag2']
    },
    'automatic': {
        'fqdn': 'node.example.com',
        'hostname': 'node',
        'platform': 'platform',
        'platform_family': 'family',
        'platform_version': 'version',
        'recipes': ['recipe1', 'recipe2'],
        'roles': ['role1', 'role2'],
        'kernel': {
            'machine': 'machine'
        },
        'lsb': {
            'description': 'description'
        }
    }
}]

keyerror_chef_value = [{
    'name': 'name',
    'normal': {
        'tags': ['tag1', 'tag2']
    },
    'automatic': {
        'fqdn': 'node.example.com',
        'hostname': 'node',
        'platform': 'platform',
        'platform_family': 'family',
        'platform_version': 'version',
        'recipes': ['recipe1', 'recipe2'],
        'roles': ['role1', 'role2'],
        'kernel': {
            'machine': 'machine'
        },
        'lsb': {
            'description': 'description'
        }
    }
}]

expected_result = {
    'name': {
        'hostname': 'node.example.com',
        'nodename': 'node',
        'environment': 'unittest',
        'osName': 'platform',
        'osFamily': 'family',
        'osVersion': 'version',
        'osArch': 'machine',
        'recipes': 'recipe1,recipe2',
        'roles': 'role1,role2',
        'tags': 'recipe1,recipe2,role1,role2,tag1,tag2',
        'chef_tags': 'tag1,tag2',
        'description': 'description'
    }
}


class MockCache:
    def __init__(self, config):
        self.config = config

    def cache(self, plugin, data):
        pass

    def uncache(self, plugin):
        raise CacheNotFound


class TestChefImporter(unittest.TestCase):
    def setUp(self):
        self.config = config
        self.cache = MockCache(config)
        self.title = 'ChefServer'
        self.return_value = chef_value
        self.chef_importer = ChefImporter(self.title, self.config, self.cache)

    def raise_file_not_found(self, path):
        raise FileNotFoundError

    @mock.patch('rundeck_resources.chef_importer.ChefImporter.expand_paths')
    @mock.patch('rundeck_resources.chef_importer.ChefImporter.call_chef')
    def test_get_chef_nodes(self, mock_call_chef, mock_expand_paths):
        mock_expand_paths.return_value = \
            self.chef_importer.config.copy()
        mock_call_chef.return_value = chef_value
        result = self.chef_importer.get_chef_nodes()
        self.assertEqual(result, self.return_value)

        # Test FileNotFoundError
        mock_expand_paths.side_effect = self.raise_file_not_found
        result = self.chef_importer.get_chef_nodes()
        self.assertDictEqual(result, {})

    @mock.patch('rundeck_resources.chef_importer.ChefAPI',
                new_callable=mock.mock_open())
    @mock.patch('rundeck_resources.chef_importer.Search')
    def test_call_chef(self, mock_search, mock_chef_api):
        mock_search.return_value = chef_value
        config = self.chef_importer.config
        result = self.chef_importer.call_chef(config)
        self.assertEqual(result, self.return_value)

        mock_chef_api.assert_called_with(config['url'],
                                         config['user_cert_path'],
                                         config['user'],
                                         version=config['version'],
                                         ssl_verify=config['ssl_cert_path'])

        # Testing no SSL
        config = config_no_ssl
        result = self.chef_importer.call_chef(config)
        mock_chef_api.assert_called_with(config['url'],
                                         config['user_cert_path'],
                                         config['user'],
                                         version=config['version'],
                                         ssl_verify=False)

        # Testing no version
        config = config_no_version
        result = self.chef_importer.call_chef(config)
        mock_chef_api.assert_called_with(config['url'],
                                         config['user_cert_path'],
                                         config['user'],
                                         version='0.10.8',
                                         ssl_verify=config['ssl_cert_path'])

        # Testing no SSL and no version
        config = config_no_ssl_no_version
        result = self.chef_importer.call_chef(config)
        mock_chef_api.assert_called_with(config['url'],
                                         config['user_cert_path'],
                                         config['user'],
                                         version='0.10.8',
                                         ssl_verify=False)

    @mock.patch('rundeck_resources.chef_importer.'
                'ChefImporter.expand_user_cert_path')
    @mock.patch('rundeck_resources.chef_importer.'
                'ChefImporter.expand_ssl_cert_path')
    def test_expand_paths(self, mock_escp, mock_eucp):
        # Test wit SSL
        config = self.chef_importer.config.copy()
        unchanged_config = self.chef_importer.config.copy()
        result = self.chef_importer.expand_paths(config)
        self.assertEqual(result, config)
        mock_eucp.assert_called_with(unchanged_config['user_cert_path'])
        mock_escp.assert_called_with(unchanged_config['ssl_cert_path'])

        # Test no SSL
        config = config_no_ssl.copy()
        result = self.chef_importer.expand_paths(config)
        config['ssl_cert_path'] = None
        self.assertEqual(result, config)
        mock_eucp.assert_called_with(unchanged_config['user_cert_path'])
        mock_escp.assert_called_with(None)

    @mock.patch('rundeck_resources.chef_importer.check_file')
    def test_expand_user_cert_path(self, mock_cf):
        path = "/"
        mock_cf.return_value = "/"
        result = self.chef_importer.expand_user_cert_path("/")
        self.assertEqual(result, path)
        mock_cf.assert_called_with(path)

    @mock.patch('rundeck_resources.chef_importer.check_file')
    def test_expand_ssl_cert_path(self, mock_cf):
        path = "/"
        mock_cf.return_value = "/"
        result = self.chef_importer.expand_ssl_cert_path(path)
        self.assertEqual(result, path)
        mock_cf.assert_called_with(path)

        # Testing file not found
        mock_cf.side_effect = check_file
        result = self.chef_importer.expand_ssl_cert_path(path)
        self.assertEqual(result, '')
        mock_cf.assert_called_with(path)

    @mock.patch('rundeck_resources.chef_importer.ChefImporter.get_chef_nodes')
    def test_import_resources(self, mock_chef_nodes):
        mock_chef_nodes.return_value = chef_value
        self.assertEqual(
            self.chef_importer.import_resources(), expected_result)

        # Testing username case
        username_value = expected_result.copy()
        username_value['name'].update({
            'username': 'rundeck',
        })
        self.chef_importer.config = config_rundeck_user_login
        self.assertEqual(
            self.chef_importer.import_resources(), username_value)

        # Testing KeyError Exception
        mock_chef_nodes.return_value = keyerror_chef_value
        self.assertEqual(
            self.chef_importer.import_resources(), {})

        # Testing AttributeError Exception
        mock_chef_nodes.return_value = AttrError()
        self.assertEqual(
            self.chef_importer.import_resources(), {})


class AttrError:
    def __iter__(self):
        raise AttributeError
