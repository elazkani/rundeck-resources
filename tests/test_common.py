import unittest
from unittest import mock
from rundeck_resources.common import check_file


class TestCommon(unittest.TestCase):
    @mock.patch('os.path')
    def test_check_file(self, mock_os):
        test_path = '.'
        mock_os.isfile.return_value = False
        mock_os.exists.return_value = False
        self.assertRaises(FileNotFoundError, check_file, test_path)
