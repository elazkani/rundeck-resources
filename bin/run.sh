#!/bin/ash

if !(echo $ARGS | grep -q "V"); then 
    if [ -z ${CONFIG} ]; then
        echo "'CONFIG' environment variable not set"
        exit 1
    fi
fi

rundeck-resources $ARGS $CONFIG
